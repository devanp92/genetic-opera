package code;


import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

/**
 * Created by devan on 4/4/14.
 */
public class RunScraper {
    public static void main(String[] args) {
        File xmlFile = new File("/Users/devan/Desktop/iTunes_copy.xml");
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);
            doc.getDocumentElement().normalize();
            System.out.println("Root element: " + doc.getDocumentElement().getNodeName());

//            printAttributes(doc.getElementsByTagName("dict"));



        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//    public static void printAttributes(NodeList nodeList){
//        for (int i = 0; i < nodeList.getLength(); i++) {
//            Node nNode = nodeList.item(i);
//            if(nNode.getNodeType() == Node.ELEMENT_NODE){
//                if(nNode.hasAttributes()){
//                    NamedNodeMap nodeMap = nNode.getAttributes();
//
//                    for (int j = 0; j < nodeMap.getLength(); j++) {
//                        Node node = nodeMap.item(j);
//                        if(node.getNodeName().equalsIgnoreCase("Artist") && node.getNodeValue().equalsIgnoreCase("Slightly Stoopid")){
//                            System.out.println(node.getTextContent());
//                        }
//
//                    }
//
//                }
//            }
//        }
    }


